<?php

use Magento\Framework\Component\ComponentRegistrar;

ComponentRegistrar::register(
    ComponentRegistrar::THEME,
    'adminhtml/OooAst/theme-main',
    __DIR__
);
